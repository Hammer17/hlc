import logo from './logo.svg';
import './App.css';
import Menu from './navbar/navbar';
import { useState } from 'react';
import LawnCarousel from './carousel/carousel'

function App() {
  return (
  
    <div className="App">
      <LawnCarousel/>
    </div>
  );
}

export default App;
