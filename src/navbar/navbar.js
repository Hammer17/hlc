import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

const Menu = (props) => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);

  return (
    <div className="menu-navbar">
      <Navbar color="faded" light>
        <NavbarBrand href="/" className="mr-auto">HLC</NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink href="/About/">About</NavLink>
            </NavItem>
         
            <NavItem>
              <NavLink href="/Staff/">Staff</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/Contact/">Contact</NavLink>
            </NavItem>


          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Menu;