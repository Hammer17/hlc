import React, { useState } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
import lawn1 from '../images/evan-dvorkin-vFHx5UHQ9P8-unsplash.jpg'
import lawn2 from '../images/remi-muller-LlHgaeBwYVE-unsplash.jpg'
import lawn3 from '../images/sharon-mccutcheon-fSlCxR0dnZY-unsplash.jpg'

const items = [
  {
    src:lawn1,
    altText: 'Slide 1',
    caption: '1900 Hampton Way'
  },
  {
    src: lawn2,
    altText: 'Slide 2',
    caption: '1818 Rose Street.'
  },
  {
    src: lawn3,
    altText: 'Slide 3',
    caption: '15 Herring Court'
  }
];

const LawnCarousel = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  }

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  }

  const goToIndex = (newIndex) => {
    if (animating) return;
    setActiveIndex(newIndex);
  }

  const slides = items.map((item) => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <img src={item.src} alt={item.altText} />
        <CarouselCaption captionText={item.caption} captionHeader={item.caption} />
      </CarouselItem>
    );
  });

  return (
    <Carousel
      activeIndex={activeIndex}
      next={next}
      previous={previous}
    >
      <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
      {slides}
      <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
      <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
    </Carousel>
  );
}

export default LawnCarousel;