import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Staff from './Staff';
import About from './About'
import Contact from './Contact'
import Menu from './navbar/navbar';
import reportWebVitals from './reportWebVitals';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';

ReactDOM.render(
  <React.StrictMode>
      <Menu/>
  <Router>
    <Route exact path="/" component={App}/>
    <Route exact path="/About" component={About}/>
    <Route exact path="/Staff" component={Staff}/>
    <Route exact path="/Contact" component={Contact}/>
    {/* <Route exact path="/Courses" component={CourseOfferings}/> */}
  </Router>
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
