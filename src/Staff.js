import React from 'react'
import './Staff.css'
import { Container, Row, Col } from 'reactstrap';
import { Jumbotron } from 'reactstrap';

const Staff = (props)=>{
    return(
        <div className="about-item">
        <Container>
            <Row>
                <Col xs="3" className="image1">
                
                </Col>
                <Col xs="9">
       
                <div className="staff-content">
                    <Jumbotron fluid className className="staff-content">
                        <Container fluid>
                        
                        <p className="lead">
                       
                            "I take a lot of pride in my work!"
                            <br></br>
                            - Steve Infield (Lawn Specialist) 
                            </p>
                        </Container>
                    </Jumbotron>
                    </div>
                </Col>
            </Row>
        </Container>
        <Container>
            <Row>
                <Col xs="9" >
                <div className="staff-content">
                <Jumbotron fluid className className="staff-content">
                        <Container fluid>
                        <p className="lead">
                       
                       "I love our clients!"
                       <br></br>
                       - Rachel Grimm (Reception) 
                       </p>
                        </Container>
                    </Jumbotron>
                    </div>
                </Col>
                <Col xs="3" className="image2">
                   
                </Col>
            </Row>
        </Container>
        <Container>
            <Row>
                <Col xs="3" className="image3">
               
                </Col>
                <Col xs="9" >
                <div className="staff-content">
                    <Jumbotron fluid className className="staff-content">
                        <Container fluid>
                        <p className="lead">
                       
                       "This company's success stems from the amazing workers."
                       <br></br>
                       - Louie Esienbach (CEO) 
                       </p>
                        </Container>
                    </Jumbotron>
                    </div>
                </Col>
            </Row>
        </Container>

        </div>
    )
}
export default Staff;