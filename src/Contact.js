import React from 'react';
import SubmitModal from './modal/modal';
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import "./Contact.css"

const Contact = (props) => {
  return (
    <div className="contactPage">
    <Form>
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleEmail">Email</Label>
            <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
          </FormGroup>
        </Col>
      </Row>
      <FormGroup>
        <Label for="exampleAddress">Address</Label>
        <Input type="text" name="address" id="name" placeholder="1234 Main St"/>
      </FormGroup>
      <FormGroup>
        <Label for="exampleAddress">First</Label>
        <Input type="text" name="Fname" id="name" placeholder="First Name"/>
      </FormGroup>
      <FormGroup>
        <Label for="exampleAddress">Last</Label>
        <Input type="text" name="Lname" id="exampleAddress" placeholder="Last Name"/>
      </FormGroup>
      <FormGroup>
        <Label for="exampleAddress2">Address 2</Label>
        <Input type="text" name="address2" id="exampleAddress2" placeholder="Apartment, studio, or floor"/>
      </FormGroup>
      <Row form>
        <Col md={6}>
          <FormGroup>
            <Label for="exampleCity">City</Label>
            <Input type="text" name="city" id="exampleCity"/>
          </FormGroup>
        </Col>
        <Col md={4}>
          <FormGroup>
            <Label for="exampleState">State</Label>
            <Input type="text" name="state" id="exampleState"/>
          </FormGroup>
        </Col>
        <Col md={2}>
          <FormGroup>
            <Label for="exampleZip">Zip</Label>
            <Input type="text" name="zip" id="exampleZip"/>
          </FormGroup>  
        </Col>
      </Row>
      <FormGroup check>
      </FormGroup>
      <SubmitModal buttonLabel={"Get in Contact!"}/>
    </Form>
    </div>
  );
}

export default Contact;