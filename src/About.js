import React from 'react'
import './About.css'
import { Container, Row, Col } from 'reactstrap';
import { Jumbotron } from 'reactstrap';

const About = ()=>{
    return(
        <div className="about-item">
        <Container>
            <Row>
                <div className="about-title">
                <Col xs="3">
                    History
                </Col>
                </div>
                <Col xs="9">
                    <div>
                    <Jumbotron fluid className="about-content">
                        <Container fluid>
                        <p className="lead">Founded in 2020, we specialise in all things to do with lawn care. We have a 100% satisfaction guarantee!</p>
                        </Container>
                    </Jumbotron>
                    </div>
                </Col>
            </Row>
        </Container>
        <Container>
            <Row>
                <Col xs="9">
                <div>
                <Jumbotron fluid className="about-content">
                        <Container fluid>
                        <p className="lead">We are adamant about your lawn care needs. Please see our get in contact page for more information.</p>
                        </Container>
                    </Jumbotron>
                    </div>
                </Col>
                <div className="about-title">

                    <Col xs="3">
                        About
                    </Col>
                </div>

            </Row>
        </Container>
        </div>
    )
}
export default About